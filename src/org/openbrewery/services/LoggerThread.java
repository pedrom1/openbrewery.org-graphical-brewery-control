package org.openbrewery.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openbrewery.OpenBrewery;

public class LoggerThread extends Thread {
	private OpenBrewery brewery;
	private String fileName;
	private File logFile;
	private Long logInterval; // in ms
	private Boolean run = false;
	
	public LoggerThread(OpenBrewery brewery, String fileName, Long logInterval)
	{
		this.brewery = brewery;
		this.fileName = fileName;
		this.logInterval = logInterval;
	}
	

	public void run() {
		System.out.println("++ LoggerThread RUN started++ ");
		run = true;
		
		
		try {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
		DateFormat dateFormatHuman = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		// TODO: write the config file map

		logFile = new File(this.fileName + "_" + dateFormat.format(date) + ".csv"); // create file
		
		if (!logFile.exists()) {
			logFile.createNewFile();
		}
		
		FileWriter fw = new FileWriter(logFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		//bw.write("Brewing started at " + dateFormatHuman.format(date));
		

		// TODO: fix headers to match # sensors etc defined in config
		bw.append("time,temp1,temp2,temp3,temp4,l1,l2,h1,h2,h3,h4,vlv1,vlv2,vlv3,pvlv1,pvlv2,pvlv3,p1,p2,p3\r\n");
		
		while(run) {
			try {
				Thread.sleep(this.logInterval);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("Logging...");
			bw.append(dateFormatHuman.format(new Date())+",");
			
			for(int i =0; i < brewery.getTempSensors().size(); i++) {
				bw.append(brewery.getTempSensors().get(i).getTemperature()+",");
			}
			
			//  level
			bw.append(brewery.level[0]+",");
			bw.append(brewery.level[1]+",");
			
			bw.append(brewery.heater_state[0]+",");
			bw.append(brewery.heater_state[1]+",");
			bw.append(brewery.heater_state[2]+",");
			bw.append(brewery.heater_state[3]+",");
			
			bw.append(brewery.onoff_valve_state[0]+",");
			bw.append(brewery.onoff_valve_state[1]+",");
			bw.append(brewery.onoff_valve_state[2]+",");
			
			bw.append(brewery.valve_prop_state[0]+",");
			bw.append(brewery.valve_prop_state[1]+",");
			bw.append(brewery.valve_prop_state[2]+",");
						
			bw.append(brewery.pump_state[0]+",");
			bw.append(brewery.pump_state[1]+",");
			bw.append(brewery.pump_state[2]+"\r\n");
			bw.flush();
		}
		bw.append("Brewing ended at " + dateFormatHuman.format(new Date()));
		bw.close();
		System.out.println("Done");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void cancel() {
		run = false;
		System.out.println(" LOGGER ++ CANCEL ++ ");	
	}
	
	
	public void interrupt() {
		run = false;
		System.out.println("Logger Interrupted!");
	}
}
