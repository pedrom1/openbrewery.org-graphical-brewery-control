/*

function logMessage(message) {
    // create a toast:
    ngToast.create(message);
}

*/
function toastProcess(process) {
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": true,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "0",
	  "extendedTimeOut": "0",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	};
	toastr.info("<div class='progress'>"+
	  "<div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='45' aria-valuemin='0' aria-valuemax='100' style='width: 45%'>"+
		"<span class='sr-only'>45% Complete</span>"+
	 " </div>"+
	"</div>", process);
}

function toastSuccess(message) {
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "2000",
      "extendedTimeOut": "10000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "slideUp"
    };

    toastr.success(message, "Success");
	$("#log").append("<span class='logNormal'>"+message+"</span><br/>");
}

function toastError(message) {
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": true,
      "onclick": null,
	  "timeOut":"0", /* make errors sticky */
	  "extendedTimeOut": "0",/* make errors sticky */
      "showEasing": "swing",
      "hideEasing": "swing",
	  "hideDuration": "50",
      "showMethod": "fadeIn",
      "hideMethod": "hide",
	  "closeButton":true
    };

    toastr.error(message, "Error");
	$("#log").append("<span class='logError'>"+message+"</span><br/>");
}

function toastWarning(message) {
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "500",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "slideUp"
    };

    toastr.warning(message, "Warning");
	$("#log").append("<span class='logWarning'>"+message+"</span><br/>");
}

// Get the plumbing going - connect piping
	
	jsPlumb.ready(function() {
			// Get an instance and configure it
			var instance = jsPlumb.getInstance({
				// default drag options
				DragOptions : { cursor: 'pointer', zIndex:2000 },
				// the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
				// case it returns the 'labelText' member that we set on each connection in the 'init' method below.
				ConnectionOverlays : [
					[ "Arrow", { location:1 } ],
					/* Turn off labels [ "Label", { 
						location:0.1,
						id:"label",
						cssClass:"aLabel"
					}]*/
				],
				Container:"flowchart-demo"
			});

			/* Configure styles */
			
			// this is the paint style for the connecting lines..
			var connectorPaintStyle = {
				lineWidth:4,
				strokeStyle:"#61B7CF",
				joinstyle:"round",
				outlineColor:"white",
				outlineWidth:2
			},
			// .. and this is the hover style. 
			connectorHoverStyle = {
				lineWidth:4,
				strokeStyle:"#216477",
				outlineWidth:2,
				outlineColor:"white"
			},
			endpointHoverStyle = {
				fillStyle:"#216477",
				strokeStyle:"#216477"
			},
			// the definition of source endpoints (the small blue ones)
			sourceEndpoint = {
				endpoint:"Dot",
				paintStyle:{ 
					strokeStyle:"#7AB02C",
					fillStyle:"transparent",
					radius:4,
					lineWidth:3 
				},				
				isSource:true,
				connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
				connectorStyle:connectorPaintStyle,
				hoverPaintStyle:endpointHoverStyle,
				connectorHoverStyle:connectorHoverStyle,
				dragOptions:{},
				overlays:[
					[ "Label", { 
						location:[0.5, 1.5], 
						label:"Drag",
						cssClass:"endpointSourceLabel" 
					} ]
				]
			},		
			// the definition of target endpoints (will appear when the user drags a connection) 
			targetEndpoint = {
				endpoint:"Dot",					
				paintStyle:{ fillStyle:"#7AB02C",radius:11 },
				hoverPaintStyle:endpointHoverStyle,
				maxConnections:-1,
				dropOptions:{ hoverClass:"hover", activeClass:"active" },
				isTarget:true,			
				overlays:[
					[ "Label", { location:[0.5, -0.5], label:"Drop", cssClass:"endpointTargetLabel" } ]
				]
			},			
			
			// read this http://www.jsplumb.org/doc/basic-concepts.html
			
			// Define listener to set properties for new connections
			// Basically, make curved connectors and hide all the endpoints
			init = function(connection) {			
				/*connection.getOverlay("label").setLabel(connection.sourceId.substring(15) + "-" + connection.targetId.substring(15));
				connection.bind("editCompleted", function(o) {
					if (typeof console != "undefined")
						console.log("connection edited. path is now ", o.path);
				});*/
			};			
/*
			var _addEndpoints = function(toId, sourceAnchors, targetAnchors) {
					for (var i = 0; i < sourceAnchors.length; i++) {
						var sourceUUID = toId + sourceAnchors[i];
						instance.addEndpoint("flowchart" + toId, sourceEndpoint, { anchor:sourceAnchors[i], uuid:sourceUUID });						
					}
					for (var j = 0; j < targetAnchors.length; j++) {
						var targetUUID = toId + targetAnchors[j];
						instance.addEndpoint("flowchart" + toId, targetEndpoint, { anchor:targetAnchors[j], uuid:targetUUID });						
					}
				};*/

			// suspend drawing and initialise.
			instance.doWhileSuspended(function() {

				/*_addEndpoints("HLT", ["TopCenter", "BottomCenter"], ["LeftMiddle", "RightMiddle"]);			
				_addEndpoints("Kettle", ["LeftMiddle", "BottomCenter"], ["TopCenter", "RightMiddle"]);
				_addEndpoints("Mash", ["RightMiddle", "BottomCenter"], ["LeftMiddle", "TopCenter"]);
				_addEndpoints("Fermentor", ["LeftMiddle", "RightMiddle"], ["TopCenter", "BottomCenter"]);
					*/		
					
				// listen for new connections; initialise them the same way we initialise the connections at startup.
				instance.bind("connection", function(connInfo, originalEvent) { 
					init(connInfo.connection);
				});			
							
				// make all the window divs draggable - TURNED OFF
				//instance.draggable(jsPlumb.getSelector(".flowchart-demo .window"), { grid: [20, 20] });		
				
				
				// THIS DEMO ONLY USES getSelector FOR CONVENIENCE. Use your library's appropriate selector 
				// method, or document.querySelectorAll:
				//jsPlumb.draggable(document.querySelectorAll(".window"), { grid: [20, 20] });
				
				// connect a few up
				/*instance.connect({
					source:"pump3-view", 
					target:"valve3-view",  
					connector: ["Flowchart"],
					anchors:["Right", "Left"]	
					});
				instance.connect({
					source:"tankv1-view", 
					target:"pump3-view",  
					cornerRadius: "10px",
					connector: ["Flowchart"],
					anchors:["Right", "Top"]
					});*/
				//instance.connect({uuids:["HLTRightMiddle", "KettleLeftMiddle"], editable:false});
				//instance.connect({uuids:["KettleRightMiddle", "MashLeftMiddle"], editable:true});
				//instance.connect({uuids:["MashBottomCenter", "FermentorRightMiddle"], editable:true});
				
				//instance.connect({uuids:["Window3RightMiddle", "Window2RightMiddle"], editable:true});
				//instance.connect({uuids:["Window4BottomCenter", "Window1TopCenter"], editable:true});
				//instance.connect({uuids:["Window3BottomCenter", "Window1BottomCenter"], editable:true});
				//
				
				//
				// listen for clicks on connections, and offer to delete connections on click.
				//
				/*instance.bind("click", function(conn, originalEvent) {
					if (confirm("Delete connection from " + conn.sourceId + " to " + conn.targetId + "?"))
						jsPlumb.detach(conn); 
				});	
				
				instance.bind("connectionDrag", function(connection) {
					console.log("connection " + connection.id + " is being dragged. suspendedElement is ", connection.suspendedElement, " of type ", connection.suspendedElementType);
				});		
				
				instance.bind("connectionDragStop", function(connection) {
					console.log("connection " + connection.id + " was dragged");
				});

				instance.bind("connectionMoved", function(params) {
					console.log("connection " + params.connection.id + " was moved");
				});*/
			});

			jsPlumb.fire("jsPlumbDemoLoaded", instance);
			window.jsPlumbInstance=instance;
	
		});


/*** MAIN() type of deal here ***/
$().ready(function() {
	

	// The Application
	// ---------------
	var pump3=null;
	var valve3=null;
	var propvalve1=null, tank1=null, heater1=null;
	var brewery=null;
	// Our overall **AppView** is the top-level piece of UI.
	var AppView = Backbone.View.extend({

		// Instead of generating a new element, bind to the existing skeleton of
		// the App already present in the HTML.
		el: $("#openbreweryapp"),

		// Delegated events for filling or emptying
		events: {
			//"click #fill": "fill",
			//    "click #empty": "empty"
		},
		
		// At initialization we bind to the relevant events on the `Todos`
		// collection, when items are added or changed. Kick things off by
		// loading any preexisting todos that might be saved in *localStorage*.
		initialize: function () {
			// First, prevent Ajax calls from being cached [note turned cache false off since arduino no likey, hmm].  Cache == bad for this application.  Also, long timeouts are unnecessary.
			$.ajaxSetup(
				{ 
					//cache: false,
					timeout: 30000 //Time in milliseconds
				}
			);
			
			this.footer = this.$('footer');
			this.main = $('#main');
			// fetch data, if appropriate
			
			// Copy this (the app) to a variable named that.  When created functions within a function, "this" refers to the function not that app anymore, so if we want to refer to the app, we need "that"
			var that=this;
			
			// Create an Arduino object and save it in the application (yup, can only control one arduino at once for the time being)
			// The remaining data is automatically loaded from the Arduino itself
			this.arduino = new openbrewery.components.arduino.board({
				name: 'my Arduino Mega',
				ip: 'localhost',
				port:8887  // note that security is off. not a great idea for production
			});
			
			// if the arduino loads, create the brewery.  if not, show an error message.
			var arduinoLoaded = function() { 
				
				// TODO: *** load brewery from config file here - uh, maybe just use urlRoot from backbone, yes?
				if(!window.brewery) {
					// Remove the event listener because it is no longer needed
					that.arduino.get("io").removeEventListener(IOBoardEvent.READY, this);
					
					toastSuccess("Connected to '"+ that.arduino.get("name") +"'"); 
					that.createBrewery();
				}
			};
			var arduinoDidNotLoad = function() { 
				console.log("Couldnt connect to Arduino"); 
				toastError("Failed to connect to Arduino '"+ that.arduino.get("name") +"' at " + that.arduino.get("ip") + ":" + that.arduino.get("port")); 
			};
			
			// Somehow the namespace is broken in Breakout, so need to create a local reference to BO namespace
			var IOBoardEvent = BO.IOBoardEvent;
			var io = this.arduino.get("io");
			io.addEventListener(IOBoardEvent.READY, arduinoLoaded);
			
			// load pins once; otherwise, nothing displays since createBrewery is not called
			//that.arduino.updatePins(arduinoLoaded, arduinoDidNotLoad);
			
			// Create a function to poll the Arduino and tell it to get new data for it's pins
			/*window.updatePins = function() { 
				console.log("Refreshing IO..."); that.arduino.updatePins(arduinoLoaded, arduinoDidNotLoad); 
				//setTimeout(window.updatePins, 15000); 
			};
			window.updatePins();*/
			
			

		},
		
		createBrewery: function() {
			/* 
			
				Brewery
			
			*/
			
			this.brewery = new openbrewery.components.brewery({name:"Motherlode", arduino: this.arduino});
			window.brewery = this.brewery;
			var breweryView = new openbrewery.views.brewery({
				model: this.brewery,
				id: this.brewery.attributes.name
			});
			this.main = this.main.append(breweryView);
			
			var ioboard = this.arduino.get("io");
			
			/* 
			
				City Water
			
			*/
			this.brewery.addComponent( // not connected for now - city water in - pin8,  heatex city water - pin 7, recirc - pin 6
				{
					container: this.main,
					model: openbrewery.components.valve,
					view: openbrewery.views.valve,
					modelProperties: {
						name: "cityWater", 
						prettyName: "City Water",
						css:{top:"20em",left:"1em"},
						io: ioboard.getDigitalPin(8), 
						board: ioboard,
						initialize: function(model) {
								model.get("board").setDigitalPinMode(8, BO.Pin.DOUT);
								
								if(model.get("io").getType()!=BO.Pin.DOUT)
									toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								//var ioboard = attrs["io"];
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						},						
					}
				}
			);
			
			////'background-image': backgroundImageStyleProperty
			
			/* 
			
				HLT parts 
			
			*/
			var hlt = this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.tank, 
					view: openbrewery.views.tank, 
					modelProperties: {
						name: "hlt",
						prettyName: "HLT",
						level:0,
						ioLevel: ioboard.getAnalogPin(0), // level: A0 A2
						ioTemperature:  ioboard.getAnalogPin(4), // temp : A4 6 8 10  
						css: {top:"15em",left:"20em"},
						board: ioboard,
						initialize: function(model) {
							// level
							model.get("board").enableAnalogPin(0);
							
							// temp
							model.get("board").enableAnalogPin(4);
							
							if(model.get("ioLevel").getType()!=BO.Pin.AIN || model.get("ioTemperature").getType()!=BO.Pin.AIN)
								toastError("Failed to setup AIN for " + model.get("name"));
							
							
							// Listen for changes in readings
							model.get("ioLevel").addEventListener(BO.PinEvent.CHANGE, function(evt) { model.set("level", model.onLevelChange(evt)); });
							model.get("ioTemperature").addEventListener(BO.PinEvent.CHANGE, function(evt) { model.set("temperature", model.onTemperatureChange(evt)); });
							
							/*model.get("ioLevel").addEventListener(
								BO.PinEvent.CHANGE, 
								function(evt) {
									console.log(evt.target.value);
										model.updateLevel(evt.target.value);
										
								});*/
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.heater, 
					view: openbrewery.views.heater, 
					modelProperties: {
						name: "hltHeater1",
						prettyName: "HLT H1",
						css: {top:"3em",left:"13em"},
						io: ioboard.getDigitalPin(2), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(2, BO.Pin.DOUT); 
							
							if(model.get("io").getType()!=BO.Pin.DOUT)
								toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.heater, 
					view: openbrewery.views.heater, 
					modelProperties: {
						name: "hltHeater2",
						prettyName: "HLT H2",
						css: {top:"3em",left:"27em"},
						io: ioboard.getDigitalPin(3), // should be 7
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(3, BO.Pin.DOUT);// should be 7
							
							if(model.get("io").getType()!=BO.Pin.DOUT)
								toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.pump, 
					view: openbrewery.views.pump, 
					modelProperties: {
						name: "hltPump",
						prettyName: "HLT Pump",
						css: {top:"45em",left:"20em"},
						io: ioboard.getDigitalPin(17), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(17, BO.Pin.DOUT);
							
							if(model.get("io").getType()!=BO.Pin.DOUT)
								toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.proportionalValve, 
					view: openbrewery.views.proportionalValve, 
					modelProperties: {
						name: "hltPropValve",
						prettyName: "HLT Vlv",
						css: {top:"45em",left:"37em"},
						io: ioboard.getDigitalPin(12),
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(12, BO.Pin.PWM);
							
							if(model.get("io").getType()!=BO.Pin.PWM)
								toastError("Failed to setup PWM for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.currentPWM)
									pin.value=attrs.currentPWM*pin.analogWriteResolution;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			/* 
			
				Mash parts 
			
			*/
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.tank, 
					view: openbrewery.views.tank, 
					modelProperties: {
						name: "mash",
						prettyName: "Mash",
						level:0,
						ioTemperature:  ioboard.getAnalogPin(6), // temp : A4 6 8 10  
						css: {top:"15em",left:"55em"},
						board: ioboard,
						initialize: function(model) {
							
							
							// temp
							model.get("board").enableAnalogPin(6);
							
							if(model.get("ioTemperature").getType()!=BO.Pin.AIN)
								toastError("Failed to setup AIN for " + model.get("name"));
							
							
							// Listen for changes in readings
							model.get("ioTemperature").addEventListener(BO.PinEvent.CHANGE, function(evt) { model.set("temperature", model.onTemperatureChange(evt)); });
							
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.pump, 
					view: openbrewery.views.pump, 
					modelProperties: {
						name: "mashPump",
						prettyName: "Mash Pump",
						css: {top:"45em",left:"55em"},
						io: ioboard.getDigitalPin(15), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(15, BO.Pin.DOUT);
							
							if(model.get("io").getType()!=BO.Pin.DOUT)
								toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.proportionalValve, 
					view: openbrewery.views.proportionalValve, 
					modelProperties: {
						name: "mashPropValve",
						prettyName: "Mash Vlv",
						css: {top:"45em",left:"72em"},
						io: ioboard.getDigitalPin(13),
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(13, BO.Pin.PWM);
							
							if(model.get("io").getType()!=BO.Pin.PWM)
								toastError("Failed to setup PWM for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.currentPWM)
									pin.value=attrs.currentPWM*pin.analogWriteResolution;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			/* 
			
				Kettle parts 
			
			*/
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.tank, 
					view: openbrewery.views.tank, 
					modelProperties: {
						name: "kettle",
						prettyName: "Kettle",
						level:0,
						ioLevel: ioboard.getAnalogPin(2), // level: A0 A2
						ioTemperature:  ioboard.getAnalogPin(8), // temp : A4 6 8 10  
						css: {top:"15em",left:"90em"},
						board: ioboard,
						initialize: function(model) {
							// level
							model.get("board").enableAnalogPin(2);
							
							// temp
							model.get("board").enableAnalogPin(8);
							
							if(model.get("ioLevel").getType()!=BO.Pin.AIN || model.get("ioTemperature").getType()!=BO.Pin.AIN)
								toastError("Failed to setup AIN for " + model.get("name"));
							
							
							// Listen for changes in readings
							model.get("ioLevel").addEventListener(BO.PinEvent.CHANGE, function(evt) { model.set("level", model.onLevelChange(evt)); });
							model.get("ioTemperature").addEventListener(BO.PinEvent.CHANGE, function(evt) { model.set("temperature", model.onTemperatureChange(evt)); });
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.heater, 
					view: openbrewery.views.heater, 
					modelProperties: {
						name: "kettleHeater1",
						prettyName: "Kettle H1",
						css: {top:"3em",left:"83em"},
						io: ioboard.getDigitalPin(4), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(4, BO.Pin.DOUT);
							
							if(model.get("io").getType()!=BO.Pin.DOUT)
								toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			); 
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.heater, 
					view: openbrewery.views.heater, 
					modelProperties: {
						name: "kettleHeater2",
						prettyName: "Kettle H2",
						css: {top:"3em",left:"97em"},
						io: ioboard.getDigitalPin(5), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(5, BO.Pin.DOUT);
							
							if(model.get("io").getType()!=BO.Pin.DOUT)
								toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.pump, 
					view: openbrewery.views.pump, 
					modelProperties: {
						name: "kettlePump",
						prettyName: "Kettle Pump",
						css: {top:"45em",left:"90em"},
						io: ioboard.getDigitalPin(19), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(19, BO.Pin.DOUT);
							
							if(model.get("io").getType()!=BO.Pin.DOUT)
								toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.proportionalValve, 
					view: openbrewery.views.proportionalValve, 
					modelProperties: {
						name: "kettlePropValve",
						prettyName: "Kettle Vlv",
						css: {top:"45em",left:"117em"},
						io: ioboard.getDigitalPin(10), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(10, BO.Pin.PWM);
							
							if(model.get("io").getType()!=BO.Pin.PWM)
								toastError("Failed to setup PWM for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.currentPWM)
									pin.value=attrs.currentPWM*pin.analogWriteResolution;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.valve, 
					view: openbrewery.views.valve, 
					modelProperties: {
						name: "kettleRecirculate",
						prettyName: "Recirculate Vlv",
						css: {top:"20em",left:"117em"},
						io: ioboard.getDigitalPin(6), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").setDigitalPinMode(6, BO.Pin.DOUT);
							
							if(model.get("io").getType()!=BO.Pin.DOUT)
								toastError("Failed to setup IO for " + model.get("name"));
						},
						save: function(attrs) {
							try { 
								var pin = this.get("io");
								if(attrs.status)
									pin.value=BO.Pin.HIGH;
								else
									pin.value=BO.Pin.LOW;
								this.set(attrs);
								console.log(this.get("tag") + " Success saving " + this.get("name"));
								return this;									
							} catch(e) {
								console.log(this.get("tag") + " Error saving " + this.get("name"));
								return this;
							}
						}
					}
				}
			);
			
			
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.heatExchanger, 
					view: openbrewery.views.heatExchanger, 
					modelProperties: {
						name: "heatEx",
						prettyName: "Heat Ex",
						css: {top:"30em",left:"137em"},
						ioTemperature: ioboard.getAnalogPin(10), 
						board: ioboard,
						initialize: function(model) {
							model.get("board").enableAnalogPin(10);
							
							if(model.get("ioTemperature").getType()!=BO.Pin.AIN)
								toastError("Failed to setup AIN for " + model.get("name"));
							else
								model.set("color",onColor);
							
							// Listen for changes in readings
							model.get("ioTemperature").addEventListener(BO.PinEvent.CHANGE, function(evt) { model.set("temperature", model.onTemperatureChange(evt)); });
						}
					}
				}
			);
			console.log("Done creating brewery");
			
			/* fake component 
			this.brewery.addComponent(
				{ 
					container: this.main,
					model: openbrewery.components.pump, 
					view: openbrewery.views.pump, 
					properties: {
						
						
						css: {top:"45em",left:"117em"}
					}
				}
			);*/
			

			// Connect components together
			window.jsPlumbInstance.connect({
					source:"cityWater", 
					target:"hlt",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Right", "Left"] // use the right hand side of cityWater to connect to the Left hand side of hlt	
					});
			window.jsPlumbInstance.connect({
					source:"hlt", 
					target:"hltPump",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Bottom", "Top"]	
					});
			window.jsPlumbInstance.connect({
					source:"hltPump", 
					target:"hltPropValve",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Right", "Left"]	
					});
			window.jsPlumbInstance.connect({
					source:"hltPropValve", 
					target:"mash",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Top", "Left"]	
					});
			window.jsPlumbInstance.connect({
					source:"mash", 
					target:"mashPump",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Bottom", "Top"]	
					});
			window.jsPlumbInstance.connect({
					source:"mashPump", 
					target:"mashPropValve",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Right", "Left"]	
					});
			window.jsPlumbInstance.connect({
					source:"mashPropValve", 
					target:"kettle",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Top", "Left"]	
					});		
			window.jsPlumbInstance.connect({
					source:"kettle", 
					target:"kettlePump",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Bottom", "Top"]	
					});	
			window.jsPlumbInstance.connect({
					source:"kettlePump", 
					target:"kettlePropValve",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Right", "Left"]	
					});	
			window.jsPlumbInstance.connect({
					source:"kettlePump", 
					target:"kettleRecirculate",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Right", "Left"]	
					});	
			window.jsPlumbInstance.connect({
					source:"kettleRecirculate", 
					target:"kettle",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Top", "Right"]	
					});	
			window.jsPlumbInstance.connect({
					source:"kettlePropValve", 
					target:"heatEx",  
					//endpoint:[ "Blank" ],
					connector: ["Flowchart"],
					anchors:["Right", "Left"]	
					});	
		},

		// Re-rendering the App just means refreshing the statistics -- the rest
		// of the app doesn't change.
		render: function () {
			// nothing for now
		},

		// Add a single todo item to the list by creating a view for it, and
		// appending its element to the `<ul>`.
		fill: function (todo) {
			/*var view = new TodoView({
				model: todo
			});
			this.$("#todo-list").append(view.render().el);*/
		},


		// Clear all done todo items, destroying their models.
		empty: function () {
			//foo
			return false;
		},

		saveAll: function () {
			/*var done = this.allCheckbox.checked;
			Todos.each(function (todo) {
				todo.save({
					'done': done
				});
			});*/
		}

	});

	// Finally, we kick things off by creating the **App**.
	var App = new AppView({});
});	
	
	
	





















// the tank filling state machine
var tank = Stately.machine({
    'EMPTY': {
        'fill': /* => */
            'FILLING'
    },
        'FILLING': {
        'isFull': function () {
            this.level = (this.level === undefined ? 0 : ++this.level);
            return this.level >= 2 ? this.FULL : this.FILLING;
        },
            'empty': /* => */
            'EMPTYING'
    },
        'FULL': {
        'empty': /* => */
            'EMPTYING'
    },
        'EMPTYING': {
        'isEmpty': function () {
            this.level = (this.level === undefined ? 0 : --this.level);
            return this.level == 0 ? this.EMPTY : this.EMPTYING;
        },
            'fill': /* => */
            'FILLING'
    }
});

function testStately() {
//the initial state of the door is open (it's the first state object)
console.log("Is the tank empty?");
console.log(tank.getMachineState() === 'EMPTY'); // true;

console.log("Let's fill the tank.  Is the tank filling?");
tank.fill();
console.log(tank.getMachineState() === 'FILLING'); // true;

console.log("Is the tank full?");
tank.isFull();
console.log(tank.getMachineState() === 'FULL'); // false;

console.log("How about now?");
tank.isFull();
console.log(tank.getMachineState() === 'FULL'); // false;

console.log("How about now?");
tank.isFull();
console.log(tank.getMachineState() === 'FULL'); // true;

console.log("let's empty the tank.  Is it emptying?");
tank.empty();
console.log(tank.getMachineState() === 'EMPTYING'); // true;

console.log("Is the tank empty?");
tank.isEmpty();
console.log(tank.getMachineState() === 'FULL'); // false;

console.log("Let's fill the tank.  Is the tank filling?");
tank.fill();
console.log(tank.getMachineState() === 'FILLING'); // true;
}